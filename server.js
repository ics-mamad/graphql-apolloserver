const express = require("express");
const { ApolloServer } = require("@apollo/server");
const { expressMiddleware } = require("@apollo/server/express4");
const bodyParser = require("body-parser");
const cors = require("cors");
const DB = require("./db");
const userResolver = require("./Resolvers/userResolver");
const userTypedefs = require("./TypeDefs/userTypedefs");


async function startApolloServer() {
    DB()
  const app = express();
  const server = new ApolloServer({
    typeDefs:userTypedefs,
    resolvers: userResolver
  });
  app.use(bodyParser.json());
  app.use(cors());
  await server.start();

  app.use("/graphql", expressMiddleware(server));

  app.listen(4000, () => {
    console.log(`🚀 Server ready at http://localhost:4000/graphql`);
  })
}

startApolloServer();
