const UserSchema = require('../Models/userSchema')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');
const userSchema = require('../Models/userSchema');
SECRET_KEY = "RESTAURENTAPI";

const userResolver = {
    Query: {
        getUsers: async () => {
            try {
                const users = await UserSchema.find();
                return users;
            } catch (error) {
                console.error("Error fetching users:", error);
                throw new Error("Failed to fetch users.");
            }
        }
    },
    Mutation: {
        createUser: async (parent, args) => {
            const { name, age, gender, email } = args;
            try {
                const existingUser = await UserSchema.findOne({ email });
                if (existingUser) {
                    throw new Error("This email is already in use.");
                }

                //hash password
                const hashedPassword = await bcrypt.hash(args.password, 12);

                const user = new UserSchema({
                    name, age, gender, email, password: hashedPassword
                });
                await user.save();

                //create token
                const token = jwt.sign(
                    { email: user.email, id: user._id },
                    SECRET_KEY
                );
                

                const { password, ...rest } = user.toObject(); // Convert to object to exclude mongoose-specific properties
                const displayData = { ...rest, token };

                return displayData;
            } catch (error) {
                console.error("Error creating user:", error);
                throw new Error("Failed to create user.");
            }
        },
        updateUser: async (parent, args) => {
            const { id } = args;
            try {
                await UserSchema.findByIdAndUpdate(id, args);
                const user = await UserSchema.findById(id);
                if (!user) {
                    throw new Error("User not found.");
                }
                return user;
            } catch (error) {
                console.error("Error updating user:", error);
                throw new Error("Failed to update user.");
            }
        },
        deleteUser: async (parent, args) => {
            const { id } = args;
            try {
                const user = await UserSchema.findByIdAndDelete(id);
                if (!user) {
                    throw new Error("User not found.");
                }
                return user;
            } catch (error) {
                console.error("Error deleting user:", error);
                throw new Error("Failed to delete user.");
            }
        }
    }
}

module.exports = userResolver;
