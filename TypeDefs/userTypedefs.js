
const userTypedefs = `
type User{
    name:String
    gender:String
    age:Int
    email:String
    password:String
}

type Query{
    getUsers:[User]   
}
type Mutation{
    createUser(name: String!,age: Int!,gender: String!,email: String!,password: String!):User
    updateUser(id:ID,name: String,age: Int,gender: String,email: String,password: String):User
    deleteUser(id:ID):User
}
`

module.exports = userTypedefs